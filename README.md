<h1>Accelerated Web Pages</h1>
<br>
O objetivo do Google com o projeto é garantir que as páginas web carreguem quase instantaneamente ao clicar nalgum link.
<br><br>
O projeto é open-source de grandes editores de conteúdo e empresas de tecnologia, com o objetivo de melhorar todo o ecossistema de conteúdo para dispositivos móveis.
Basicamente, uma página AMP possui uma arquitetura que prioriza a velocidade de carregamento da página. Esta arquitetura é dividida em 3 configurações diferentes:
<br>
<ul>
<li><strong>AMP HTML:</strong> um código HTML diferente, com algumas restrições e extensões, indo além do HTML básico. A maioria da suas tags são normais de HTML, mas algumas são substituídas por tags específicas do AMP;</li>
<li><strong>AMP JS:</strong> responsável por assegurar a renderização rápida de páginas AMP. A sua principal função é tornar assíncrono tudo o que é externo, para que nenhum elemento da página possa bloquear a renderização de outro;</li>
<li><strong>Google AMP Cache:</strong> é opcional, mas armazena todas as páginas em AMP HTML em cache nos servidores do Google e melhora o seu desempenho automaticamente. Outras empresas também podem desenvolver seu próprio cache de AMP.</li>
</ul>
<br>
Ao momento AMP é um recurso especificamente direcionado para sites que publicam conteúdo e não por exemplo e-commerce, porém isso é algo que pode mudar no futuro.
<br><br>
Várias empresas estão a refazer de raiz os seus sites, no entanto muitas tomaram outras 2 abordagens, alterar só parte do site (maioritariamente a que mais recursos têm) ou mesmo uma implementação gradual/progressiva.
<br><br>

<h1>Progressive Web Apps</h1>
<br><strong>Progressive</strong><br>
By definition, a progressive web app must work on any device and enhance progressively, taking advantage of any features available on the user’s device and browser.
<br><br><strong>Discoverable</strong><br>
Because a progressive web app is a website, it should be discoverable in search engines. This is a major advantage over native applications, which still lag behind websites in searchability.
<br><br><strong>Linkable</strong><br>
As another characteristic inherited from websites, a well-designed website should use the URI to indicate the current state of the application. This will enable the web app to retain or reload its state when the user bookmarks or shares the app’s URL.
<br><br><strong>Responsive</strong><br>
A progressive web app’s UI must fit the device’s form factor and screen size.
<br><br><strong>App-like</strong><br>
A progressive web app should look like a native app and be built on the application shell model, with minimal page refreshes.
<br><br><strong>Connectivity-independent</strong><br>
It should work in areas of low connectivity or offline (our favorite characteristic).
<br><br><strong>Re-engageable</strong><br>
Mobile app users are more likely to reuse their apps, and progressive web apps are intended to achieve the same goals through features such as push notifications.
<br><br><strong>Installable</strong><br>
A progressive web app can be installed on the device’s home screen, making it readily available.
<br><br><strong>Fresh</strong><br>
When new content is published and the user is connected to the Internet, that content should be made available in the app.
<br><br><strong>Safe</strong><br>
Because a progressive web app has a more intimate user experience and because all network requests can be intercepted through service workers, it is imperative that the app be hosted over HTTPS to prevent man-in-the-middle attacks.
<br><br>

<h3>Pontos Negativos</h3>
Os PWAs ainda não tem o controle total sobre o hardware do device: bluetooth, lista de contatos e NFC, são alguns exemplos de features que não conseguem ser acessadas pelos Progressive Web Apps.
<br>
Apesar de Google, Microsoft e Mozilla estarem apostando alto nos PWAs, <s>a Apple ainda não está</s>.
<br>
Ainda existem duas features importantes não suportadas pelo Safari: push notifications e <s>funcionamento offline</s>.
Mas, a Apple já deve estar a implementar estas tecnologias, algumas já constam em nightly builds do Safari,
<br>
Este processo pode ser consultado aqui: <a href="https://jakearchibald.github.io/isserviceworkerready/">Is Service Worker Ready?</a>
<br>
<br>
<h1>Outras Referências</h1>
<br>
<ul>
<li><a href="https://www.youtube.com/embed/1QILz1lAzWY"><strong>Why Build Progressive Web Apps?<strong></a></li>
<li><a href="https://ampbyexample.com"><strong>AMP by Example<strong></a></li>
<li><a href="https://www.youtube.com/embed/MxTaDhwJDLg">Flipkart Lite - exemplo de web app</a></li>
<li><a href="https://www.pedrodias.net/mobile/amp-pages">O que é AMP e o que você realmente precisa saber?</a></li>
<li><a href="https://www.smashingmagazine.com/2016/12/progressive-web-amps/">What Are Progressive Web AMPs?</a></li>
<li><a href="https://cloudfour.com/thinks/ios-doesnt-support-progressive-web-apps-so-what/">iOS doesn’t support Progressive Web Apps, so what?</a></li>
</ul>
