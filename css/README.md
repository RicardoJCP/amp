

<a href="http://tutorialzine.com/2016/09/everything-you-should-know-about-progressive-web-apps/">Everything You Should Know About Progressive Web Apps</a>

<strong>Progressive</strong>
<br>
By definition, a progressive web app must work on any device and enhance progressively, taking advantage of any features available on the user’s device and browser.
<br><br>
<strong>Discoverable</strong>
<br>
Because a progressive web app is a website, it should be discoverable in search engines. This is a major advantage over native applications, which still lag behind websites in searchability.
<br><br>
<strong>Linkable</strong>
<br>
As another characteristic inherited from websites, a well-designed website should use the URI to indicate the current state of the application. This will enable the web app to retain or reload its state when the user bookmarks or shares the app’s URL.
<br><br>
<strong>Responsive</strong>
<br>
A progressive web app’s UI must fit the device’s form factor and screen size.
<br><br>
<strong>App-like</strong>
<br>
A progressive web app should look like a native app and be built on the application shell model, with minimal page refreshes.
<br><br>
<strong>Connectivity-independent</strong>
<br>
It should work in areas of low connectivity or offline (our favorite characteristic).
<br><br>
<strong>Re-engageable</strong>
<br>
Mobile app users are more likely to reuse their apps, and progressive web apps are intended to achieve the same goals through features such as push notifications.
<br><br>
<strong>Installable</strong>
<br>
A progressive web app can be installed on the device’s home screen, making it readily available.
<br><br>
<strong>Fresh</strong>
<br>
When new content is published and the user is connected to the Internet, that content should be made available in the app.
<br><br>
<strong>Safe</strong>
<br>
Because a progressive web app has a more intimate user experience and because all network requests can be intercepted through service workers, it is imperative that the app be hosted over HTTPS to prevent man-in-the-middle attacks.
